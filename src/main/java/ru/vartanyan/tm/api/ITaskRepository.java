package ru.vartanyan.tm.api;

import ru.vartanyan.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> finaAllByProjectId(String projectId);

    List<Task> removeAllByProjectId(String projectId);

    Task bindTaskByProjectId(String projectId, String taskId);

    Task unbindTaskFromProject(String projectId, String taskId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task add(Task task);

    Task remove(Task task);

    void clear();

}
