package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.IProjectRepository;
import ru.vartanyan.tm.api.IProjectTaskService;
import ru.vartanyan.tm.api.ITaskRepository;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    final private ITaskRepository taskRepository;

    final private IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.finaAllByProjectId(projectId);
    }

    @Override
    public Task bindTaskByProjectId(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.bindTaskByProjectId(projectId, taskId);
    }

    @Override
    public Task unbindTaskFromProject(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.unbindTaskFromProject(projectId, taskId);
    }

    @Override
    public Project removeProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeOneById(projectId);
    }

}
